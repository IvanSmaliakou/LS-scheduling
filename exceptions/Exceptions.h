//
// Created by Ivan Smaliakou on 13.05.21.
//

#ifndef LAB2_EXCEPTIONS_H
#define LAB2_EXCEPTIONS_H

#include <string>
#include <utility>
#include <exception>

using namespace std;

class NodeNotFoundException : exception {
private:
    string exceptionString;
public:
    explicit NodeNotFoundException(string ex) {
        this->exceptionString = std::move("node not found: " + ex);
    }

    const char *what() const noexcept override {
        return this->exceptionString.c_str();
    }
};

#endif //LAB2_EXCEPTIONS_H
