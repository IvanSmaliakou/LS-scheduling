#include <iostream>
#include "model/Graph.h"
#include "model/Step.h"
#include "model/LSStep.h"

const vector<vector<int>> GRAPH_INIT_MATRIX = {
        {0, 1, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 1, 0, 0, 0, 0},
        {0, 0, 0, 1, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 1, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 1},
        {0, 0, 0, 0, 0, 0, 1, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 1, 1},
        {0, 0, 0, 0, 0, 0, 0, 0, 1},
        {0, 0, 0, 0, 0, 0, 0, 0, 0}
};

const vector<char> OPS_VECTOR = {'+', '*', '+', '*', '+', '+', '*', '+', '+'};

int main() {
    map<char, int> procsPerType = {{'+', 1},
                                   {'*', 1}};
    Graph *graph = new Graph(GRAPH_INIT_MATRIX, OPS_VECTOR, procsPerType, 9);
    int stNo = 0;
    while (graph->NextStep()) {
        cout << endl << "step " << stNo << ": (";
        LSStep *step = graph->GetStep();
        cout << "nodes: [";
        for (Node n: step->step.GetNodes()) {
            cout << "{ id: " << n.getId() << n.getOperation() << "} ";
        }
        cout << "]; remaining nodes: [";
        for (int nId: step->remainingNodes) {
            cout << "{ id: " << nId << "} ";
        }
        cout << "]; scheduled nodes: [";
        for (int nId: step->readyNodes) {
            cout << "{ id: " << nId << "} ";
        }
        cout << "];";
        stNo++;
    }
    return 0;
}
