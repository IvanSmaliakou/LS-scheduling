//
// Created by Ivan Smaliakou on 15.05.21.
//

#ifndef INC_3_LSSTEP_H
#define INC_3_LSSTEP_H

#include "Step.h"

using namespace std;

class LSStep {
public:
    vector<int> remainingNodes;
    vector<int> readyNodes;
    Step step;

    LSStep() = default;

    LSStep(Step &s, vector<int> remainingNodes, vector<int> readyNodes) {
        this->step = s;
        this->readyNodes = readyNodes;
        this->remainingNodes = remainingNodes;
    }
};

#endif //INC_3_LSSTEP_H
