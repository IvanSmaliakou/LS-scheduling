//
// Created by Ivan Smaliakou on 12.05.21.
//

#ifndef LAB2_NODE_H
#define LAB2_NODE_H


#include <string>
#include <utility>
#include <vector>

using namespace std;

class Node {
private:
    int id;
    bool critical;
    char operation;
    vector<int> inputNodes;
public:
    bool scheduled;
    bool executed;

    Node() = default;

    Node(int id, vector<int> &in, char op) {
        this->id = id;
        this->inputNodes = in;
        this->operation = op;
        this->critical = false;
        this->scheduled = false;
        this->executed = false;
    }

    void SetCritical(bool val) {
        this->critical = val;
    }

    bool IsCritical() {
        return this->critical;
    }

    int getId() {
        return this->id;
    }

    char getOperation() {
        return this->operation;
    }

    vector<int> getInputNodes() {
        return this->inputNodes;
    }
};


#endif //LAB2_NODE_H
