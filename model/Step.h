//
// Created by Ivan Smaliakou on 15.05.21.
//

#ifndef INC_3_STEP_H
#define INC_3_STEP_H
class Step {
private:
    vector<Node> nodes;
public:
    void AddNode(const Node &n) {
        this->nodes.push_back(n);
    }

    vector<Node> GetNodes() {
        return this->nodes;
    }
};
#endif //INC_3_STEP_H
