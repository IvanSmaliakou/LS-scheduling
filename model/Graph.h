//
// Created by Ivan Smaliakou on 12.05.21.
//

#ifndef LAB2_GRAPH_H
#define LAB2_GRAPH_H

#include "Node.h"
#include "LSStep.h"
#include "Step.h"
#include "../exceptions/Exceptions.h"

#include <map>

class Graph {
private:
    vector<Node> nodes;
    Step currentStep;
    map<char, int> procsPerType;
    LSStep *state = new LSStep();

    Node getNodeById(int id) {
        for (Node n: this->nodes) {
            if (n.getId() == id) {
                return n;
            }
        }
        throw NodeNotFoundException("id: " + std::to_string(id));
    }

    bool criticalNodeExists(vector<Node> nodes) {
        for (Node node:nodes) {
            if (node.IsCritical()) {
                return true;
            }
        }
        return false;
    }

    void defineCriticalPath(int lastNodeId) {
        vector<Node> initNodes;
        vector<int> currentCriticalPath;

        for (Node node: this->nodes) {
            if (node.getInputNodes().empty()) {
                initNodes.push_back(node);
            }
        }
        for (Node initNode: initNodes) {
            map<int, vector<int>> passedNodes;
            Node currentNode = initNode;
            vector<int> currentPath;
            bool passedAllChain1Time = false;
            while (true) {
                bool noOutputNodeExists = true;
                Node *alreadyIncludedNode = nullptr;
                bool alreadyIncluded = false;
                for (Node &node: this->nodes) {
                    vector<int> inputNodes = node.getInputNodes();
                    bool isOutputNode = std::count(inputNodes.begin(), inputNodes.end(),
                                                   currentNode.getId());
                    alreadyIncluded = std::count(passedNodes[currentNode.getId()].begin(),
                                                 passedNodes[currentNode.getId()].end(), node.getId());
                    if (isOutputNode && alreadyIncluded) {
                        alreadyIncludedNode = &node;
                    }
                    if (isOutputNode && !alreadyIncluded) {
                        passedNodes[currentNode.getId()].push_back(node.getId());
                        currentPath.push_back(currentNode.getId());

                        noOutputNodeExists = false;
                        passedAllChain1Time = false;
                        currentNode = node;
                        break;
                    }
                }
                if (alreadyIncludedNode != nullptr && noOutputNodeExists) {
                    currentPath.push_back(currentNode.getId());
                    currentNode = *alreadyIncludedNode;
                    noOutputNodeExists = false;
                }
                if (currentNode.getId() == lastNodeId) {
                    if (passedAllChain1Time) {
                        currentPath.clear();
                        break;
                    }
                    currentPath.push_back(lastNodeId);
                    if (currentPath.size() > currentCriticalPath.size()) {
                        currentCriticalPath = currentPath;
                    }
                    currentNode = initNode;
                    currentPath.clear();
                    passedAllChain1Time = true;
                    continue;
                }
                if (noOutputNodeExists) {
                    currentPath.clear();
                    break;
                }
            }
            if (currentPath.size() > currentCriticalPath.size()) {
                currentCriticalPath = currentPath;
            }
        }
        for (Node &n: this->nodes) {
            if (std::count(currentCriticalPath.begin(), currentCriticalPath.end(), n.getId())) {
                n.SetCritical(true);
            }
        }
    }

    bool allNodesScheduled() {
        for (const Node &n: this->nodes) {
            if (!n.scheduled) {
                return false;
            }
        }
        return true;
    }

    void removeNodeById(vector<int> &a, vector<int> &b, int val) {
        for (int i = 0; i < a.size(); i++) {
            if (a[i] == val) {
                a.erase(a.begin() + i);
            }
        }
        for (int i = 0; i < b.size(); i++) {
            if (b[i] == val) {
                b.erase(b.begin() + i);
            }
        }
    }

public:
    Graph() = default;

    explicit Graph(vector<Node> n) {
        this->nodes = std::move(n);
    }

    explicit Graph(const vector<vector<int>> &matrix, const vector<char> &ops_vector,
                   const map<char, int> &procsPerType,
                   int size) {
        for (int i = 0; i < size; ++i) {
            vector<int> prevNodes = {};
            for (int j = 0; j < size; ++j) {
                int val = matrix[j][i];
                if (val == 1) {
                    prevNodes.push_back(j + 1);
                }
            }
            Node *n = new Node(i + 1, prevNodes, ops_vector[i]);
            this->AddNode(*n);
        }
        this->procsPerType = procsPerType;
        defineCriticalPath(matrix.size());
    }

    void AddNode(const Node &n) {
        this->nodes.push_back(n);
    }

    bool NextStep() {
        vector<int> concatedNodes = this->state->readyNodes;
        concatedNodes.insert(concatedNodes.end(),
                             this->state->remainingNodes.begin(),
                             this->state->remainingNodes.end());
        map<char, int> procsAvailable(this->procsPerType);
        Step *step = new Step();
        // critical nodes are prioritized
        for (int readyNodeId: concatedNodes) {
            Node readyNode = getNodeById(readyNodeId);
            if (readyNode.IsCritical() && procsAvailable[readyNode.getOperation()] > 0) {
                for (Node &allNode: this->nodes) {
                    if (allNode.getId() == readyNodeId) {
                        allNode.executed = true;
                        break;
                    }
                }
                step->AddNode(readyNode);
                removeNodeById(this->state->readyNodes, this->state->remainingNodes, readyNodeId);
                procsAvailable[readyNode.getOperation()]--;
            }
        }
        for (int readyNodeId: concatedNodes) {
            Node readyNode = getNodeById(readyNodeId);
            if (procsAvailable[readyNode.getOperation()] > 0) {
                for (Node &allNode: this->nodes) {
                    if (allNode.getId() == readyNodeId) {
                        allNode.executed = true;
                        break;
                    }
                }
                step->AddNode(readyNode);
                removeNodeById(this->state->readyNodes, this->state->remainingNodes, readyNodeId);
                procsAvailable[readyNode.getOperation()]--;
            }
        }
        this->state->step = *step;

        this->state->remainingNodes.insert(this->state->remainingNodes.end(),
                                           this->state->readyNodes.begin(),
                                           this->state->readyNodes.end());
        this->state->readyNodes.clear();

        // scheduling part
        for (Node &node: this->nodes) {
            if (node.scheduled) {
                continue;
            }
            bool allInputsExecuted = true;
            for (int inputNodeId: node.getInputNodes()) {
                Node inputNode = this->getNodeById(inputNodeId);
                if (!inputNode.executed) {
                    allInputsExecuted = false;
                    break;
                }
            }
            if (allInputsExecuted) {
                this->state->readyNodes.push_back(node.getId());
            }
        }
        for (int nodeId: this->state->readyNodes) {
            for (Node &allNode: this->nodes) {
                if (allNode.getId() == nodeId) {
                    allNode.scheduled = true;
                    break;
                }
            }
        }
        return !(this->state->readyNodes.empty() && this->state->remainingNodes.empty() && step->GetNodes().empty());
    }

    LSStep *GetStep() {
        return this->state;
    }
};


#endif //LAB2_GRAPH_H
